package manager;

import entity.local.DTUTransaction;
import dtu.ws.fastmoney.Transaction;
import dtu.ws.fastmoney.User;
import entity.local.MerchantTransaction;
import messaging.entities.EVENT_TYPE;
import messaging.entities.Event;
import messaging.entities.EventProcessor;


import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class DTUTransactionManager implements EventProcessor {
    private static List<DTUTransaction> transactions = new ArrayList<DTUTransaction>();

    public void addDTUTransaction(DTUTransaction transaction){
        transactions.add(transaction);
}

    public List<DTUTransaction> getTransactionsCustomer(String cid){
        return transactions
                .stream()
                .filter(t -> t.getCid().equals(cid))
                .collect(Collectors.toList());
    }

    public List<MerchantTransaction> getTransactionsMerchant(String mid){
        return transactions
                .stream()
                .filter(t -> t.getMid().equals(mid))
                .map(t -> toMerchantTransaction(t))
                .collect(Collectors.toList());
    }
    public List<DTUTransaction> getAllTransactions(){
        return transactions;
    }

    public MerchantTransaction toMerchantTransaction(DTUTransaction dtut)
    {
        MerchantTransaction mt = new MerchantTransaction();
        mt.setMid(dtut.getMid());
        mt.setAmount(dtut.getAmount());
        mt.setDescription(dtut.getDescription());
        mt.setTokenId(dtut.getTokenId());
        mt.setMonth(dtut.getMonth());

        return mt;
    }

    public void generateDTUTransaction(String cid, String mid, String tid, String description, BigDecimal amount)
    {

        Date date = new Date();
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        int month = localDate.getMonthValue();

        // Generate DTUTransaction for report
        DTUTransaction dtuTransaction = new DTUTransaction();
        dtuTransaction.setCid(cid);
        dtuTransaction.setMid(mid);
        dtuTransaction.setDescription(description);
        dtuTransaction.setTokenId(tid);
        dtuTransaction.setMonth(month);
        dtuTransaction.setAmount(amount);

        addDTUTransaction(dtuTransaction);
    }

    @Override
    public void processMessage(String owner, EVENT_TYPE type, Object content, String extra) throws Exception {
        if(type == EVENT_TYPE.NEW_TRANSACTION){
            var newTransaction = (DTUTransaction) content;
            generateDTUTransaction(newTransaction.getCid(), newTransaction.getMid(),
                    newTransaction.getTokenId(), newTransaction.getDescription(),
                    newTransaction.getAmount());
            System.out.println("New transaction added in the report: "+newTransaction);
        }
    }
}
