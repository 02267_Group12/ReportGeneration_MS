package messaging.mq;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;
import messaging.entities.Event;
import messaging.entities.EventProcessor;
import messaging.entities.EventReceiver;
import messaging.utils.JsonUtils;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class MQListener implements EventReceiver {

    private static final String EXCHANGE_NAME = "topic_logs";
    private static final String HOSTNAME = "g-12.compute.dtu.dk";
    private EventProcessor processor;
    private String owner;

    public MQListener(String owner, EventProcessor processor){
        this.owner = owner;
        this.processor = processor;
    }

    public void listen(String[] topics){
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(HOSTNAME);
        Connection connection = null;
        try {
            connection = factory.newConnection();
            Channel channel = connection.createChannel();
            channel.exchangeDeclare(EXCHANGE_NAME, "topic");
            String queueName = channel.queueDeclare().getQueue();

            if (topics.length < 1) {
                System.err.println("Usage: ReceiveLogsTopic [binding_key]...");
                System.exit(1);
            }

            for (String bindingKey : topics) {
                channel.queueBind(queueName, EXCHANGE_NAME, bindingKey);
            }

            System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

            DeliverCallback deliverCallback = (consumerTag, delivery) -> {
                String message = new String(delivery.getBody(), "UTF-8");
                Event event = (Event) JsonUtils.fromJson(message, Event.class);
                System.out.println(owner+" - [receive event] "+event);
                try {
                    receiveEvent(event);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            };
            channel.basicConsume(queueName, true, deliverCallback, consumerTag -> { });

        } catch (IOException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void receiveEvent(Event event) throws Exception {
        var content = JsonUtils.fromJson(event.getContent(), Class.forName(event.getContent_class()));
        processor.processMessage(owner, event.getEventType(), content, event.getExtra());
    }

    @Override
    public Event receiveAndReplyBack(Event event) {
        return null;
    }
}
