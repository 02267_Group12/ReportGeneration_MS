package org.acme;

import entity.local.DTUTransaction;
import entity.local.MerchantTransaction;
import manager.DTUTransactionManager;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/report")
public class ReportResource {

    private static DTUTransactionManager transactionManager = new DTUTransactionManager();

    @GET
    @Path("/customer/{cid}")
    public Response getCustomerReport(@PathParam("cid") String cid){
        List<DTUTransaction> transactions = transactionManager.getTransactionsCustomer(cid);
        if (!transactions.isEmpty()){
            return Response
                    .ok(new GenericEntity<List<DTUTransaction>>(transactions){})
                    .build();
        } else {
            return Response
                    .status(404, "Failed to find customer with cid: " + cid)
                    .entity("Failed to find customer with cid: " + cid)
                    .build();
        }
    }

    @GET
    @Path("/merchant/{mid}")
    public Response getMerchantReport(@PathParam("mid") String mid){
        List<MerchantTransaction> transactions = transactionManager.getTransactionsMerchant(mid);
        if (!transactions.isEmpty()){
            return Response
                    .ok(new GenericEntity<List<MerchantTransaction>>(transactions){})
                    .build();
        } else {
            return Response
                    .status(404, "Failed to find merchant with cid: " + mid)
                    .entity("Failed to find merchant with cid: " + mid)
                    .build();
        }
    }

    @GET
    @Path("/manager")
    public Response getManagerReport(){
        List<DTUTransaction> transactions = transactionManager.getAllTransactions();
        return Response
                .ok(new GenericEntity<List<DTUTransaction>>(transactions){})
                .build();
    }



}
