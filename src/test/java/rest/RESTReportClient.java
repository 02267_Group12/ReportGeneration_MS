package rest;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class RESTReportClient {
    WebTarget baseUrl;
    public RESTReportClient() {
        Client client = ClientBuilder.newClient();
        baseUrl = client.target("http://g-12.compute.dtu.dk:5001/");
    }

    public Response getCustomerReport(String cid){
        return baseUrl
                .path("report")
                .path("customer")
                .path(cid)
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .get();
    }


    public Response getMerchantReport(String mid){
        return baseUrl
                .path("report")
                .path("merchant")
                .path(mid)
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .get();
    }

    public Response getManagerReport(){
        return baseUrl
                .path("report")
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .get();
    }

}
