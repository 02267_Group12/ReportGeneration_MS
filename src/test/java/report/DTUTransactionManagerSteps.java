package report;

import entity.local.DTUTransaction;
import entity.local.MerchantTransaction;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import manager.DTUTransactionManager;
import org.junit.jupiter.api.Assertions;
import rest.RESTReportClient;


import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.util.List;

public class DTUTransactionManagerSteps {
    DTUTransactionManager dtuTransactionManager = new DTUTransactionManager();
    List<DTUTransaction> beforeTrans;
    List<DTUTransaction> beforeTransRest;
    List<DTUTransaction> filteredByCustomer;
    List<MerchantTransaction> filteredByMerchant;
    RESTReportClient clientReport = new RESTReportClient();


    @Given("a list of DTUTransactions")
    public void aListOfDTUTransactions() {

        beforeTrans = dtuTransactionManager.getAllTransactions();
        // Response response = clientReport.getManagerReport();
        // beforeTransRest = response.readEntity(new GenericType<List<DTUTransaction>>() {});
    }

    @When("a new payment is processed")
    public void aNewPaymentIsProcessed() {
        dtuTransactionManager.generateDTUTransaction("foo","bar", "baz","test transaction", new BigDecimal(100));
    }

    @Then("the new DTUtransaction is added to the list")
    public void theNewDTUtransactionIsAddedToTheList() {
        List<DTUTransaction> afterTrans = dtuTransactionManager.getAllTransactions();
        boolean isPresent = afterTrans.stream().filter(t -> t.getDescription() == "test transaction").findFirst().isPresent();
        Assertions.assertTrue(isPresent);
    }

    @And("the new DTUtransaction is added to the list and is accessible from the endpoint")
    public void theNewDTUtransactionIsAddedToTheListAndIsAccessibleFromTheEndpoint() {
        Response response = clientReport.getManagerReport();
        List<DTUTransaction> transactions = response.readEntity(new GenericType<List<DTUTransaction>>() {});
        boolean isPresent = transactions.stream().filter(t -> t.getDescription() == "test transaction").findFirst().isPresent();
        Assertions.assertTrue(isPresent);
    }

    @Given("a payment from customer {string}")
    public void aPaymentFromCustomer(String cid) {
        dtuTransactionManager.generateDTUTransaction(cid, "fooBar", "token123", "test transaction from "+ cid, new BigDecimal(100));
    }

    @When("the list of dtu transactions is filtered by customer {string}")
    public void theListOfDtuTransactionsIsFilteredByCustomer(String cid) {
        filteredByCustomer = dtuTransactionManager.getTransactionsCustomer(cid);
    }

    @Then("all resulting transactions are from customer {string}")
    public void allResultingTransactionsAreFromCustomer(String cid) {
        boolean allMatch = filteredByCustomer.stream().allMatch(t -> t.getCid().equals(cid));
        Assertions.assertTrue(allMatch);
    }

    @And("all resulting transactions are from customer {string} and are accessible from the endpoint")
    public void allResultingTransactionsAreFromCustomerAndAreAccessibleFromTheEndpoint(String cid) {
        Response response = clientReport.getCustomerReport(cid);
        var transactions = response.readEntity(new GenericType<List<DTUTransaction>>() {});
        Assertions.assertTrue(transactions.stream().allMatch(t -> t.getCid().equals(cid)));
    }


    @Given("a payment from merchant {string}")
    public void aPaymentFromMerchant(String mid) {
        dtuTransactionManager.generateDTUTransaction("BarBaz", mid, "token456", "test transaction to "+ mid, new BigDecimal(100));
    }

    @When("the list of dtu transactions is filtered by merchant {string}")
    public void theListOfDtuTransactionsIsFilteredByMerchant(String mid) {
        filteredByMerchant = dtuTransactionManager.getTransactionsMerchant(mid);
    }

    @Then("all resulting transactions are from merchant {string}")
    public void allResultingTransactionsAreFromMerchant(String mid) {
        boolean allMatch = filteredByMerchant.stream().allMatch(t -> t.getMid().equals(mid));
        Assertions.assertTrue(allMatch);

    }

    @And("all resulting transactions are from merchant {string} and are accessible from the endpoint")
    public void allResultingTransactionsAreFromMerchantAndAreAccessibleFromTheEndpoint(String mid) {
        Response response = clientReport.getMerchantReport(mid);
        var transactions = response.readEntity(new GenericType<List<DTUTransaction>>() {});
        Assertions.assertTrue(transactions.stream().allMatch(t -> t.getCid().equals(mid)));
    }
}
