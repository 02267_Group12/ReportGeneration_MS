Feature: DTUTransaction Manager

  Scenario: Storing a new DTU transaction
    Given a list of DTUTransactions
    When a new payment is processed
    Then the new DTUtransaction is added to the list
    # And the new DTUtransaction is added to the list and is accessible from the endpoint

  Scenario: Filtering transactions by customer id
    Given a payment from customer "cid1"
    When the list of dtu transactions is filtered by customer "cid1"
    Then all resulting transactions are from customer "cid1"
    # And all resulting transactions are from customer "cid1" and are accessible from the endpoint

  Scenario: Filtering transactions by merchant id
    Given a payment from merchant "mid1"
    When the list of dtu transactions is filtered by merchant "mid1"
    Then all resulting transactions are from merchant "mid1"
    # And all resulting transactions are from merchant "mid1" and are accessible from the endpoint
